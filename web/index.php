<?php
  //var_dump($_REQUEST);die();
  define('KEY_DELAY', 1000000 * 0.1);
  function icon($icon){
    return "<i class=\"fa fa-$icon\"></i>";
  }
  function button($key,$value,$class=''){
    return "<button name='KEY' value='KEY_$key' class='$class'>$value</button>";
  }
  if($_REQUEST['KEY']){
    switch ($_REQUEST['KEY']) {
      case 'KEY_POWER':
        $remote = 'SAMSUNG_AA59-00600A_POWER';break;
      default:
        $remote = 'SAMSUNG_AA59-00600A';break;
    }
    shell_exec('irsend SEND_ONCE '.$remote.' '.$_REQUEST['KEY']);
    die();
  }
  if($_REQUEST['STATION']){
    foreach(str_split($_REQUEST['STATION']) as $key){
      shell_exec('irsend SEND_ONCE SAMSUNG_AA59-00600A KEY_'.$key);usleep(KEY_DELAY);
    };
    shell_exec('irsend SEND_ONCE SAMSUNG_AA59-00600A KEY_SELECT');
  }
?>
<!doctype html>
<html lang="de">
<head>
  <meta charset="utf-8">
  <title>IRPI</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <style type="text/css">
    table {border-collapse: collapse;}
    table td {margin: 0; padding: 0;}
    button {
      font: 24px monospace;
      width: 2em;
      height: 2em;
      padding: 0;
      margin: 0;
      text-align: center;
      vertical-align: middle;
      cursor: pointer;
      float: left;
    }
    table td.row-4 button {width: 1.5em;height: 1.5em;}
    button.red {color: #f00;}
    button.green {color: #0f0;}
    button.blue {color: #00f;}
    button.yellow {color: #ff0;}
    @media screen and (orientation : portrait) {
      body {background: #000;padding: 0;}
      button {font-size: 56px;}
    }
    hr {clear: both;}
    .more button {width: 6em;float: none;display: block;}
  </style>
  <script type="text/javascript">
    (function($){$(document).ready(function() {
      var btn = null;
      $('button').on('click',function(event){btn = this});
      $('form').on('submit',function(event){
        var p = {KEY:btn.value};
        if(btn.name == 'STATION')p = {STATION:btn.value};
        console.log(p);
        $.post(window.location.pathname,p);
        return false;
      });
    });})(jQuery);
  </script>
</head>
<body>
  <form action="" method="POST">
    <table>
      <!-- cursor -->
      <tr>
        <td><?=button('VOLUMEUP',icon('volume-up'))?></td>
        <td><?=button('POWER',icon("power-off"))?></td>
        <td><?=button('CHANNELUP',icon("caret-square-o-up"))?></td>
      </tr>
      <tr>
        <td><?=button('MUTE',icon("volume-off"))?></td>
        <td><?=button('MENU',icon("bars"))?></td>
        <td><?=button('PREVIOUS',icon("retweet"))?></td>
      </tr>
      <tr>
        <td><?=button('VOLUMEDOWN',icon("volume-down"))?></td>
        <td><?=button('OPTION',icon("wrench"))?></td>
        <td><?=button('CHANNELDOWN',icon("caret-square-o-down"))?></td>
      </tr>
      <!-- cursor -->
      <tr>
        <td><?=button('INFO',icon("info"))?></td>
        <td><?=button('UP',icon("caret-up"))?></td>
        <td><?=button('LIST',icon("list"))?></td>
      </tr>
      <tr>
        <td><?=button('LEFT',icon("caret-left"))?></td>
        <td><?=button('SELECT',icon("check"))?></td>
        <td><?=button('RIGHT',icon("caret-right"))?></td>
      </tr>
      <tr>
        <td><?=button('BACK',icon("arrow-left"))?></td>
        <td><?=button('DOWN',icon("caret-down"))?></td>
        <td><?=button('EXIT',icon("sign-out"))?></td>
      </tr>
      <!-- numblock -->
      <tr>
        <td><?=button('1','1')?></td>
        <td><?=button('2','2')?></td>
        <td><?=button('3','3')?></td>
      </tr>
      <tr>
        <td><?=button('4','4')?></td>
        <td><?=button('5','5')?></td>
        <td><?=button('6','6')?></td>
      </tr>
      <tr>
        <td><?=button('7','7')?></td>
        <td><?=button('8','8')?></td>
        <td><?=button('9','9')?></td>
      </tr>
      <tr>
        <td><?=button('102ND',icon("ellipsis-h"))?></td>
        <td><?=button('0','0')?></td>
        <td><?=button('CYCLEWINDOWS',icon("television"))?></td>
      </tr>
      <tr>
        <td><?=button('PLAY',icon("play"))?></td>
        <td><?=button('PAUSE',icon("pause"))?></td>
        <td><?=button('STOP',icon("stop"))?></td>
      </tr>
      <tr>
        <td><?=button('REWIND',icon("backward"))?></td>
        <td></td>
        <td><?=button('FASTFORWARD',icon("forward"))?></td>
      </tr>
      <tr>
        <td colspan="3" class='row-4'>
          <?=button('RED',icon('square'),'red')?>
          <?=button('GREEN',icon('square'),'green')?>
          <?=button('YELLOW',icon('square'),'yellow')?>
          <?=button('BLUE',icon('square'),'blue')?>
        </td>
      </tr>
    </table>
    <div class='more'>
      <?=button('MEDIA','MEDIA')?>
      <?=button('SLEEP','SLEEP')?>
      <?=button('SWITCHVIDEOMODE','VIDEOMODE')?>
      <?=button('LANGUAGE','LANGUAGE')?>
      <?=button('SUBTITLE','SUBTITLE')?>
    </div>
    <hr>
    <div class='more'>
      <button name='STATION' value='1'>ARD</button>
      <button name='STATION' value='2'>ZDF</button>
      <button name='STATION' value='3'>NDR</button>
      <button name='STATION' value='60'>ARTE</button>
      <button name='STATION' value='1207'>KABEL1</button>
      <button name='STATION' value='1204'>VOX</button>
      <button name='STATION' value='1206'>PRO7</button>
      <button name='STATION' value='1203'>RTL</button>
      <button name='STATION' value='88'>SAT1</button>
      <button name='STATION' value='1220'>DMAX</button>
      <button name='STATION' value='1218'>TELE5</button>
      <button name='STATION' value='1210'>SIXX</button>
      <button name='STATION' value='13'>4</button>
      <button name='STATION' value='63'>ZDF NEO</button>
      <button name='STATION' value='12'>3SAT</button>
      <button name='STATION' value='1209'>RTL2</button>
      <button name='STATION' value='1219'>SRTL</button>
      <button name='STATION' value='70'>RTL NITRO</button>
      <button name='STATION' value='98'>PRO7 MAXX</button>
      <button name='STATION' value='6'>ZDF INFO</button>
      <button name='STATION' value='5'>ZDF KULT</button>
      <button name='STATION' value='1003'>EINS FEST</button>
      <button name='STATION' value='11'>TS24</button>
      <button name='STATION' value='10'>EINS+</button>
      <button name='STATION' value='8'>RBB</button>
      <button name='STATION' value='4'>HR</button>
      <button name='STATION' value='9'>SWR</button>
      <button name='STATION' value='30'>WDR</button>
      <button name='STATION' value='1235'>MDR</button>
      <button name='STATION' value='49'>SWF</button>
      <button name='STATION' value='50'>BR&alpha;</button>
      <button name='STATION' value='1238'>BFS</button>
      <button name='STATION' value='7'>PHOENIX</button>
      <hr>
      <button name='STATION' value='2033'>NJOY</button>
      <button name='STATION' value='2028'>NDR2</button>
      <button name='STATION' value='2110'>SUNSHINE</button>
      <button name='STATION' value='2035'>FRITZ</button>
    </div>
  </form>
</body>
</html>
