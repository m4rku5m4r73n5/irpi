# Bauteile #

* IR-Empfänger SIEMENS SFH506 ([Datenblatt](https://bitbucket.org/m4rku5m4r73n5/irpi/raw/03e859bed19cc93a71d78775c562b77db9be61a8/docs/sfh506.pdf))
* IR-Diode (unbekannt)
* NPN-Transistor SIEMENS BC547 ([Datenblatt](https://bitbucket.org/m4rku5m4r73n5/irpi/raw/03e859bed19cc93a71d78775c562b77db9be61a8/docs/))
* Widerstand 240Ω
* Raspberry PI GPIO 22 + 23

# Aufbau #

![![aufbau.JPG](https://bitbucket.org/repo/M4AGEx/images/2852550803-aufbau.JPG)](https://bitbucket.org/repo/M4AGEx/images/3919436514-aufbau.JPG)

# Schaltplan #

![infrarot_Steckplatine.png](https://bitbucket.org/repo/M4AGEx/images/114697248-infrarot_Steckplatine.png)

# Anleitungen #

[tutorials-raspberrypi.de](http://tutorials-raspberrypi.de/projekte/infrarot-steuerung-einrichten-von-lirc/)

[randomtutor.blogspot.de](http://randomtutor.blogspot.de/2013/01/web-based-ir-remote-on-raspberry-pi.html)

[alexba.in](http://alexba.in/blog/2013/01/06/setting-up-lirc-on-the-raspberrypi/)

[spech.de](http://www.spech.de/blog/article/universalfernbedienungrpi)